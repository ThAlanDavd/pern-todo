const express = require('express');
const { config } = require('./config');
const cors = require('cors');

const todoRoutes = require('./routes/todo');

const app = express();

app.use(cors());
app.use(express.json());

todoRoutes(app);

app.listen(config.port, () => {
  console.log(`Server running at http://localhost:${config.port}`);
});
