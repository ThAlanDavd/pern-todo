import React, { useState, useEffect, Fragment } from 'react';
import axios from 'axios';
import EditTodo from './EditTodo';

function ListTodos() {
  const [todos, setTodos] = useState([]);

  async function deleteTodo(id) {
    try {
      await axios({
        method: 'delete',
        url: `http://localhost:5000/api/todos/${id}`
      });

      setTodos(todos.filter(todo => todo.todo_id !== id));
    } catch (error) {
      console.error(error);
    }
  }

  async function getTodos() {
    const response = await axios.get('http://localhost:5000/api/todos');
    setTodos(response.data.data);
  }

  useEffect(() => {
    getTodos();
  }, []);

  console.log(todos);

  return(
    <Fragment>
      <table className='table mt-5'>
        <thead>
          <tr>
            <th>Description</th>
            <th>Edit</th>
            <th>Delete</th>
          </tr>
        </thead>
        <tbody>
          {todos && todos.map((todo) => (
            <tr key={todo.todo_id}>
              <td>{todo.description}</td>
              <td>
                <EditTodo todo={todo} />
              </td>
              <td>
                <button
                  className='btn btn-danger'
                  onClick={() => deleteTodo(todo.todo_id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </Fragment>
  );
};

export default ListTodos;
