import React from 'react';
import InputTodo from './InputTodo';
import ListTodos from './ListTodos';

function App() {
  return (
    <div className='container'>
      <InputTodo />
      <ListTodos />
    </div>
  );
};

export default App;
