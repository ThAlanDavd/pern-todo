const { Router } = require('express');
const pool = require('../db');

function todoRoutes(app) {
  const router = Router();
  app.use('/api/todos', router);

  router.get('/', async (req, res, next) => {
    try {
      const allTodos = await pool.query(`SELECT * FROM todo`);

      res.status(200).json({
        data: allTodos.rows,
        message: 'todos listed'
      });
    } catch (error) {
      next(error);
    }
  });

  router.get('/:id', async (req, res, next) => {
    try {
      const { id } = req.params;
      const todo = await pool.query(`SELECT * FROM todo WHERE todo_id = $1`, [id]);

      res.status(200).json({
        data: todo.rows[0],
        message: 'todo retrieved'
      });
    } catch (error) {
      next(error);
    }
  });

  router.post('/', async (req, res, next) => {
    try {
      const { description } = req.body;
      const newTodo = await pool.query(`INSERT INTO todo (description) VALUES($1) RETURNING *`, [description]);

      res.status(201).json({
        data: newTodo.rows[0].todo_id,
        message: 'todo created'
      });
    } catch (error) {
      next(error);
    }
  });

  router.put('/:id', async (req, res, next) => {
    try {
      const { id } = req.params;
      const { description } = req.body;

      await pool.query(`UPDATE todo SET description = $1 WHERE todo_id = $2`, [description, id]);

      res.status(201).json({
        data: id,
        message: 'todo updated'
      });
    } catch (error) {
      next(error);
    }
  });

  router.delete('/:id', async (req, res, next) => {
    try {
      const { id } = req.params;

      await pool.query(`DELETE FROM todo WHERE todo_id = $1`, [id]);

      res.status(200).json({
        data: id,
        message: 'todo deleted'
      });
    } catch (error) {
      next(error);
    }
  });
}

module.exports = todoRoutes;