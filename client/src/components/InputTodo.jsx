import React, { useState, Fragment } from 'react';
import axios from 'axios';

function InputTodo() {
  const [description, setDescription] = useState('');

  async function handleSubmit(e) {
    e.preventDefault();
    try {
      const body = { description };
      await axios({
        method: 'post',
        url: 'http://localhost:5000/api/todos',
        data: body
      })
      window.location = '/';
    } catch (error) {
      console.error(error);
    }
  }

  return(
    <Fragment>
      <h1 className='text-center mt-5'>PERN Stack example app</h1>
      <form className='d-flex' onSubmit={handleSubmit}>
        <input
          type='text'
          placeholder='Add todo'
          className='form-control'
          value={description}
          onChange={(e) => setDescription(e.target.value)}
        />

        <button
          className='btn btn-success'
          type='submit'
        >Add</button>
      </form>
    </Fragment>
  );
};

export default InputTodo;
